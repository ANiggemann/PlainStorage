# PlainStorage

Purpose of this microservice is to provide a simple and easy to use persistent key value storage for other microservices.
PlainStorage is capable to hold and access millions of key value pairs.
Because it is lightweight and dont need extra drivers and other external components it can be deployed and used effortlessly and in virtually no amount of time.
It can by used for big data or few configuration data sets for other microservices as well.

# Features
* Access via browser, Postman, cURL, other microservices etc. via GET and POST requests
* Persistent storage in a file
* Storing key value pairs
* Searching for keys and values with regular expressions too
* Removing of key value pairs
* Store arbitrary values and get unique keys back
* Generate test data entries (2 million entries in 10 seconds)

# Usage
* Start the PlainStorage.jar:
  * java -jar PlainStorage.jar
* Start with storage file name
  * java -jar PlainStorage.jar --storage.file=mystorage.sto
* Use Browser, Postman or cURL to fire requests (endpoints see below) against the service.

# Examples:
* Show the content of the storage:
  * localhost:8080/show
  * (Entries are only shown if up to 1000, otherwise only the number of entries will be shown)
* Write a key value pair into the storage via browser:
  * localhost:8080/wr?key=my+key&value=my+value+into+the+storage
* Write a key value pair into the storage via cURL:
  * curl -F "key=my key" -F "value=my value into the storage" localhost:8080/write
* Read a value via key in the browser:
  * localhost:8080/read?key=my+key
* Read the first value of a key that matches a regular expression in the browser:
  * localhost:8080/search?regex=my.*
* Delete a key value pair via DELETE request
  * curl -X "DELETE" localhost:8080/item/TestKey0cb0fbe6c79c4ed5877d0a04ad1d650a
* Delete (remove) a key value pair via POST request
  * curl -F "key=my key" localhost:8080/remove
* Generate 5000 test data entries via browser:
  * localhost:8080/generatetestdata?entries=5000

(Replace localhost with the IP or host name of the system where the service is running)

# JAR Command line parameter
* server.port
  * The port that PlainStorage is listening for requests
  * Default 8080
* storage.file
  * Filename of the storage, will be created if not there
  * Default tempdir/plain.sto

# Endpoints (Request method in parenthesis)
* /show, /info, /s or /i
  * Show the storage content (GET)
  * Parameter maximum (optional, default 1000)
* /readvaluebykey, /read, /get or /r
  * Read value by key (GET)
  * Parameter key
* /matchkey, /search, /find or /k
  * Find a value based on a regex on the key (GET)
  * Parameter regex
* /matchvalue, /searchvalue, /findvalue or /v
  * Find a value based on a regex on the value itself (GET)
  * Parameter regex
* /matchvaluegetkey
  * Find the key to a value based on a regex on the value itself (GET)
  * Parameter regex
* /wr
  * Write a key value pair (GET)
  * Parameter key
  * Parameter value
* /writekeyvalue, /write, /put or /w
  * Write a key value pair (POST)
  * Parameter key
  * Parameter value
* /writevalue or /getrandomkey
  * Write a value and get a unique random key (POST)
  * Parameter value
  * Parameter keyprefix (optional)
* /item/key
  * Delete a key value pair (DELETE)
  * Parameter key
* /removekeyvalue, /delete, /remove or /r
  * Delete a key value pair (POST)
  * Parameter key
* /generatetestdata
  * Generate storage entries (GET)
  * Parameter entries
    * Number of entries to generate (optional, default 400)
  * Parameter clear (optional, default True)
    * Empty the storage before generating test data

# Build PlainStorage.jar with Gradle
* Command line:
  * gradlew bootJar
