package biz.niggemann.plainstorage;

import org.springframework.boot.Banner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        final SpringApplicationBuilder sb = new SpringApplicationBuilder(Application.class);
        sb.bannerMode(Banner.Mode.OFF);
        sb.headless(false).run(args);
    }
}
