package biz.niggemann.plainstorage.controllers;

import net.openhft.chronicle.map.ChronicleMapBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentMap;


@SuppressWarnings("unused")
@RestController
public class PlainStorageController {

    private ConcurrentMap<String, String> storage = null;

    public PlainStorageController() {
        if (storageFilename == null)
            storageFilename = "";
        File storageFile = ("".equals(storageFilename)) ? new File(System.getProperty("java.io.tmpdir"), "plain.sto") : new File(storageFilename);
        try {
            storage = ChronicleMapBuilder.of(String.class, String.class).name("PlainStorage")
                    .averageKeySize(16).averageValueSize(80).entries(1_000_000L)
                    .createPersistedTo(storageFile);
            System.out.println("Storage loaded from " + storageFile.getAbsolutePath());
            System.out.println("Entries: " + storage.size());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Value("${server.port:8080}")
    private String serverPort;

    @Value("${storage.file:}")
    private String storageFilename;

    // Show storage content
    @GetMapping(value = {"/show", "/info", "/s", "/i"})
    private String show(@RequestParam(value = "maximum", required = false, defaultValue = "1000") int maximum) {
        String retVal = "PlainStorage V1.0<br><br>Storage content:<br><br>";
        StringBuilder sb = new StringBuilder();
        int i = 0;
        for (Map.Entry<String, String> entry : storage.entrySet()) {
            i++;
            sb.append(String.format("%s: %s", entry.getKey(), entry.getValue())).append("<BR>");
            if (i >= maximum) {
                sb.append(Integer.toString(i)).append(" Entries where shown...<br>");
                break;
            }
        }
        retVal += sb.toString() + "<br>Total entries: " + storage.size();
        return retVal;
    }

    // Get value for key
    @GetMapping(value = {"/readvaluebykey", "/read", "/get", "/r"})
    private String readValueByKey(@RequestParam(value = "key") String key) {
        return storage.getOrDefault(key, "");
    }

    // Get first Value for key that matches regex
    @GetMapping(value = {"/matchkey", "/search", "/find", "/k"})
    private String matchKey(@RequestParam(value = "regex") String regex) {
        String retVal = "";
        for (Map.Entry<String, String> entry : storage.entrySet()) {
            if (entry.getKey().matches(regex)) {
                retVal = entry.getValue();
                break;
            }
        }
        return retVal;
    }

    // Get value that matches regex
    @GetMapping(value = {"/matchvalue", "/searchvalue", "/findvalue", "/v"})
    private String matchValue(@RequestParam(value = "regex") String regex) {
        String retVal = "";
        for (String value : storage.values())
            if (value.matches(regex)) {
                retVal = value;
                break;
            }
        return retVal;
    }

    // Get the key for the value that matches regex
    @GetMapping(value = {"/matchvaluegetkey"})
    private String matchValueGetKey(@RequestParam(value = "regex") String regex) {
        String retVal = "";
        for (Map.Entry<String, String> entry : storage.entrySet()) {
            if (entry.getValue().matches(regex)) {
                retVal = entry.getKey();
                break;
            }
        }
        return retVal;
    }

    // Write key value pair via GET request
    @GetMapping(value = {"/wr"})
    private String writeViaGet(@RequestParam(value = "key") String gKey,
                               @RequestParam(value = "value") String gValue) {
        storage.put(gKey, gValue);
        return "OK";
    }

    // Write key value pair via POST request
    @PostMapping(value = {"/writekeyvalue", "/write", "/put", "/w"})
    private String writeKeyValue(@RequestParam(value = "key") String key,
                                 @RequestParam(value = "value") String value) {
        storage.put(key, value);
        return "OK";
    }

    // Write value and get a random key with an optional prefix
    @PostMapping(value = {"/writevalue", "/getrandomkey"})
    private String writeValue(@RequestParam(value = "value") String value,
                              @RequestParam(value = "keyprefix", required = false, defaultValue = "") String optionalKeyPrefix) {
        String key = (optionalKeyPrefix + UUID.randomUUID().toString()).trim();
        storage.put(key, value);
        return key;
    }

    // Delete key value pair
    @DeleteMapping(value = "item/{key}")
    private String removeKeyValue(@PathVariable(value = "key") String key) {
        storage.remove(key);
        return "OK";
    }

    // Delete key value pair
    @PostMapping(value = {"/removekeyvalue", "/delete", "/remove", "/r"})
    private String removeKeyValueViaPost(@RequestParam(value = "key") String key) {
        storage.remove(key);
        return "OK";
    }

    // Generate test data
    @GetMapping(value = {"/generatetestdata"})
    private String generateTestData(@RequestParam(value = "entries", required = false, defaultValue = "400") int entries,
                                    @RequestParam(value = "clear", required = false, defaultValue = "True") boolean clear) {
        if (clear)
            storage.clear();
        for (int i = 0; i < entries; i++) {
            String value = (UUID.randomUUID().toString()).replace("-", " ") + "  TESTVALUE " + Integer.toString(i);
            writeValue(value, "TestData_");
        }
        return show(1000);
    }
}
